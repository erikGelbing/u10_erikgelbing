package bowling;

import java.util.LinkedList;

public abstract class Game implements IGame {

	private static int id = 0;
	public static int pins, maxNumberOfPlayers, allowedThrows, currentRound, turns;
	static boolean GameIsRunning;

	private LinkedList<Player> allPlayers = new LinkedList<Player>();

	@Override
	public Player addPlayer(String name) {
		// TODO Auto-generated method stub
		allPlayers.add(new Player(name, id++));
		return allPlayers.get(allPlayers.size() - 1);

	}

	@Override
	public Player getActivePlayer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getActivePlayerCount() {
		// TODO Auto-generated method stub
		return allPlayers.size();
	}

	@Override
	public int getMaxPlayerCount() {
		// TODO Auto-generated method stub
		return id + 1;
	}

	@Override
	public String getName(Player aPlayer) {
		// TODO Auto-generated method stub
		return aPlayer.getName();
	}

	@Override
	public int getPinCount() {
		// TODO Auto-generated method stub
		return pins;
	}

	@Override
	public int getPinsLeft() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Player getPlayer(int id) {
		// TODO Auto-generated method stub

		for (int i = 0; i < allPlayers.size(); i++) {
			if (id == (allPlayers.get(i)).getID())
				return allPlayers.get(i);
		}
		System.out.println("The Player with the ID: " + id + " does not exist");
		return null;

	}

	@Override
	public int getRound() {
		// TODO Auto-generated method stub
		return currentRound;
	}

	@Override
	public int getRoundCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int[] getScore(Player player) {
		// TODO Auto-generated method stub
		return player.getScore();
	}

	@Override
	public int getThrow() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Player getWinner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasStarted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean startGame() {
		// TODO Auto-generated method stub

		if (GameIsRunning)
			System.out.println("The Game has already been started");
		if(allPlayers.size()<2)
			System.out.println("Less than 2 Players joined the Game. Please add more Players");
		else GameIsRunning=true;
		return GameIsRunning;
	}

	@Override
	public boolean throwBall(int count) {
		// TODO Auto-generated method stub
		return false;
	}

}
