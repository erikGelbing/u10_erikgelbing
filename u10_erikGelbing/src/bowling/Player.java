package bowling;

public class Player {

	private String name;
	private int id, score;

	/**
	 * Constructor for a Player
	 * 
	 * @param name
	 *            Player name
	 * @param id
	 *            Player ID
	 */
	Player(String name, int id) {
		this.name = name;
		this.id = id;
	}

	/**
	 * @return Player Name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return Player ID
	 */
	public int getID() {
		return id;
	}
	
	public int getScore(){
		return score;
	}

}
